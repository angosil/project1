<?php
/**
 * Created by PhpStorm.
 * User: sunand
 * Date: 29/10/16
 * Time: 16:06
 */
namespace model;

use config\mysql;
use model\post as post;

class model
{
    function open_database_connection($dsn=null)
    {
        if(is_null($dsn))
            $dsn = "mysql:host=".mysql::HOST.";dbname=".mysql::NAME.";charset=utf8";
        try {
            $link = new \PDO($dsn, mysql::USER, mysql::PASSWORD);
        } catch (\PDOException $e) {
            return 'Connection Failed: ' . $e->getMessage();
        }

        return $link;
    }

    function close_database_connection($link)
    {
        $link = null;
    }

    public function get_by_search($query,$limit=10)
    {
        $link = $this->open_database_connection();
        $stmt = $link->prepare('
            SELECT h.id, h.nombre, th.tipo, ho.n_estrellas, tha.tipo_habitacion, ap.n_apartamentos, ap.n_adultos, mu.municipio, pr.provincia FROM hospedaje h 
            LEFT JOIN tipo_hopedaje th ON h.id_tipo_hospedaje = th.id 
            LEFT JOIN hotel ho ON h.id = ho.id
            LEFT JOIN tipo_habitacion tha ON ho.tipo_habitacion = tha.id 
            LEFT JOIN apartamentos ap ON h.id = ap.id
            LEFT JOIN municipios mu ON h.ubicacion = mu.id
            LEFT JOIN provincias pr ON mu.id_provincia = pr.id
            WHERE CONCAT(h.nombre, th.tipo) LIKE :query
            ORDER BY h.nombre ASC LIMIT :limit;');
        $query = "%$query%";
        $stmt->bindValue(':query', $query, \PDO::PARAM_STR);
        $stmt->bindValue(':limit', $limit, \PDO::PARAM_INT);
        $stmt->execute();
        $result = array();
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $result[] = $row;
        }
        $this->close_database_connection($link);
        return $result;
    }
}