<?php
/**
 * Created by PhpStorm.
 * User: sunand
 * Date: 31/10/16
 * Time: 8:06
 */

namespace test;

use config\mysql;
use model\model;

require_once('config/mysql.php');
require_once('model/model.php');

class modelTest extends \PHPUnit_Framework_TestCase
{
    protected $model;

    public function setUp(){
        $this->model = new model();
    }
    public function tearDown(){ }

    public function testConnectionIsValid(){
        $model = new model();
        $this->assertTrue($model->open_database_connection() !== false);
    }

    public function testBySearch()
    {
        $stack = $this->model->get_by_search('bea');
        $this->assertNotEmpty($stack);
        $stack = $this->model->get_by_search('bea',1);
        $this->assertEquals(1,count($stack));

        return $stack;
    }
}
