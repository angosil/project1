# README #

### ¿Para qué es este repositorio? ###

Esta aplicación implementa una busqueda sencilla en PHP 7 sobre una BD en Mysql 5.6. 

### ¿Cómo me instalo? ###

* Resumen de la configuración
En la carpeta config/mysql.php se define una clase con las constantes necesarias para la conexión.
* Cómo ejecutar pruebas
Para las pruebas se utilizó PHPUnit 5.6.2. las pruebas estan en la carpeta test. 
* Instrucciones de implementación

Crear una BD utilizando el script tablas.sql que esta en la carpeta data.

Utilizar datos de pruebas que estan en el script data.sql que esta en la carpeta data.

En config/mysql.php llenar los datos necesarios para la conexión en la BD.

Correr la prueba de conexión a BD "phpunit tests/modelTest.php".

El script a correr es index.php, se le pasan 2 parametros, una palabra y un número, ejemplo 
"php index.php hotel 1", el primero es la expresion que va ser buscada en BD y el segundo el
limite de resultados a mostrar.



