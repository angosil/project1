<?php
/**
 * Created by PhpStorm.
 * User: sunand
 * Date: 29/10/16
 * Time: 14:59
 */
error_reporting(E_ALL);
ini_set("display_errors", 1);

use controllers\controller;

spl_autoload_register(function ($nombre_clase) {
    if(file_exists( $nombre_clase.'.php'))
        require_once($nombre_clase.'.php');
    else
        require_once(str_replace('\\','/',$nombre_clase).'.php');
});

if (defined('STDIN')) {
    $type = substr($argv[1],0,3);
}//else error averiguar que es la constante STDIN

$limit = 10;
if(isset($argv[2]) && intval($argv[2])>0){
    $limit = intval($argv[2]);
}

$controller = new controller();
$controller->list_action($type,$limit);
