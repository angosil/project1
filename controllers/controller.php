<?php
/**
 * Created by PhpStorm.
 * User: sunand
 * Date: 29/10/16
 * Time: 16:13
 */
namespace controllers;

use model\category;
use model\model;
use model\post;

class controller
{
    function list_action($query,$limit)
    {
        $model = new model();
        $items = $model->get_by_search($query,$limit);
        if (empty($items)){
            require 'templates/empty.php';
            return;
        }
        require 'templates/list.php';
    }
}